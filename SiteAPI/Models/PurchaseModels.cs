using System;

namespace SiteAPI.Models
{
    public class PurchaseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public DateTime PurchaseDate { get; set; }
        public bool Cancelled { get; set; }
        public DateTime? CancelledDate { get; set; }
    }
}