using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteAPI.DTO
{
    public class PurchaseDTO
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }

        [ForeignKey("Product")]
        public int ProductId { get; set; }

        public decimal Price { get; set; }
        public DateTime PurchaseDate { get; set; }
        public bool Cancelled { get; set; }
        public DateTime? CancelledDate { get; set; }

        public UserDTO User { get; set; }
        public ProductDTO Product { get; set; }
    }
}