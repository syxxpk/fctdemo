using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SiteAPI.DTO
{
    public class UserDTO
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(255)]
        public string Name { get; set; }
        [MaxLength(255)]
        public string Email { get; set; }
        [MaxLength(100)]
        public string Password { get; set; }
    }
}