using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SiteAPI.DTO
{
    public class ProductDTO
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(255)]
        public string Name { get; set; }        
        public decimal Price { get; set; }
    }
}