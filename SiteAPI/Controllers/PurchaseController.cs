using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiteAPI.DTO;
using SiteAPI.Models;

namespace SiteAPI.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class PurchaseController : ControllerBase
    {

        private readonly ProductDBContext _dbContext;

        public PurchaseController(ProductDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        //Get the users purchase list
        [HttpGet, Authorize]
        public IEnumerable<PurchaseModel> Get()
        {
            var userId = Convert.ToInt32(User.FindFirst("UserId").Value);

            return _dbContext.Purchases.Include(o => o.Product).Where(o => o.UserId == userId).Select(o => new PurchaseModel
            {
                Id = o.Id,
                Name = o.Product.Name,
                Price = o.Price,
                PurchaseDate = o.PurchaseDate,
                Cancelled = o.Cancelled,
                CancelledDate = o.CancelledDate
            });
        }

        //Purchase a product
        [HttpPost, Authorize]
        public IActionResult Post(int productId)
        {
            var userId = Convert.ToInt32(User.FindFirst("UserId").Value);
            var product = _dbContext.Products.FirstOrDefault(o => o.Id == productId);

            if (product == null)
                return NotFound();

            _dbContext.Purchases.Add(new PurchaseDTO {
                UserId = userId,
                ProductId = product.Id,
                Price = product.Price,
                PurchaseDate = DateTime.Now
            });

            _dbContext.SaveChanges();

            return Ok();
        }

        //Cancels a Purchase
        [HttpDelete, Authorize]
        public IActionResult Delete(int purchaseId)
        {
            var userId = Convert.ToInt32(User.FindFirst("UserId").Value);
            var purchase = _dbContext.Purchases.FirstOrDefault(o => o.Id == purchaseId);

            if (purchase == null)
                return NotFound();

            if (purchase.Cancelled)
                return BadRequest("Purchase already cancelled.");

            purchase.Cancelled = true;
            purchase.CancelledDate = DateTime.Now;

            _dbContext.SaveChanges();

            return Ok();
        }

    }
}