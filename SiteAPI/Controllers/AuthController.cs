using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using SiteAPI.Models;

namespace SiteAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly ProductDBContext _dbContext;

        public AuthController(ProductDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpPost, Route("login")]
        public IActionResult Login([FromBody]LoginModel user)
        {
            if (user == null)
            {
                return BadRequest("Invalid Request");
            }

            var dbUser = _dbContext.Users.FirstOrDefault(o => o.Email == user.UserName && o.Password == user.Password);

            if (dbUser != null)
            {
                var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Startup.SECRET_KEY));
                var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

                var claims = new List<Claim> 
                { 
                    new Claim(ClaimTypes.Name, dbUser.Name),
                    new Claim("UserId", dbUser.Id.ToString())
                };

                var tokeOptions = new JwtSecurityToken(
                    issuer: Startup.ISSUER,
                    audience: Startup.AUDIENCE,
                    claims: claims,
                    expires: DateTime.Now.AddMinutes(300),
                    signingCredentials: signinCredentials
                );

                var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);
                return Ok(new { Token = tokenString });
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}