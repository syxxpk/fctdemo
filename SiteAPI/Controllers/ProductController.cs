﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SiteAPI.Models;

namespace SiteAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly ProductDBContext _dbContext;

        public ProductController(ProductDBContext dbContext)
        {
            _dbContext = dbContext;
        }

         //Get the product list
        [HttpGet, Authorize]
        public IEnumerable<ProductModel> Get()
        {
            return _dbContext.Products.Select(o => new ProductModel
            {
                Id = o.Id,
                Name = o.Name,
                Price = o.Price
            });
        }        

    }
}
