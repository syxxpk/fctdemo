using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SiteAPI.DTO;

namespace SiteAPI
{
    public class ProductDBContext : DbContext
    {

        public ProductDBContext(DbContextOptions<ProductDBContext> options) : base(options) { }

        public DbSet<ProductDTO> Products { get; set; }
        public DbSet<PurchaseDTO> Purchases { get; set; }
        public DbSet<UserDTO> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserDTO>().HasData(new UserDTO { Id = 1, Name = "Test", Email = "test@nowhere.com", Password = "changeme" });

            modelBuilder.Entity<ProductDTO>().HasData(new ProductDTO { Id = 1, Name = "Product 1", Price = 10 });
            modelBuilder.Entity<ProductDTO>().HasData(new ProductDTO { Id = 2, Name = "Product 2", Price = 20 });
            modelBuilder.Entity<ProductDTO>().HasData(new ProductDTO { Id = 3, Name = "Product 3", Price = 50 });

            modelBuilder.Entity<PurchaseDTO>().HasData(new PurchaseDTO { Id = 1, UserId = 1, ProductId = 1, Price = 10, PurchaseDate = DateTime.Now.AddDays(-2) });
            modelBuilder.Entity<PurchaseDTO>().HasData(new PurchaseDTO { Id = 2, UserId = 1, ProductId = 3, Price = 50, PurchaseDate = DateTime.Now.AddDays(-1) });
        }

    }
}