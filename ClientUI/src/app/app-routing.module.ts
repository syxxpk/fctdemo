// import { NgModule } from '@angular/core';
// import { Routes, RouterModule } from '@angular/router';
// import { LoginComponent } from './login/login.component';
// import { DashboardComponent } from './dashboard/dashboard.component';
// import { AuthGuardService } from './guards/auth-guard.service';
// import { JwtModule } from "@auth0/angular-jwt";

// const routes: Routes = [
//   { path: 'login', component: LoginComponent },
//   { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService] },
// ];

// export function tokenGetter() {
//   return localStorage.getItem("jwt");
// }

// @NgModule({
//   imports: [
//     RouterModule.forRoot(routes),
//     JwtModule.forRoot({
//       config: {
//         tokenGetter: tokenGetter,
//         // whitelistedDomains: ["localhost:5000", "localhost:5001"],
//         // blacklistedRoutes: []
//       }
//     })
//   ],  
//   providers: [AuthGuardService],
//   exports: [RouterModule]
// })
// export class AppRoutingModule { }
