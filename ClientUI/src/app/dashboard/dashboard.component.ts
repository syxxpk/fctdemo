import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from "@angular/router";
import { MatTableDataSource, MatSort } from '@angular/material';
import { Product } from '../_interface/product.model';
import { Purchase } from '../_interface/purchase.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewInit {

  public productDisplayedColumns = ['name', 'price', 'purchase'];
  public productData = new MatTableDataSource<Product>();

  public purchaseDisplayedColumns = ["name", "price", "purchaseDate", "cancelled", "cancelledDate", "cancel"];
  public purchaseData = new MatTableDataSource<Purchase>();

  @ViewChild(MatSort, {static: true}) productSort: MatSort;
  @ViewChild(MatSort, {static: true}) purchaseSort: MatSort;

  constructor(private http: HttpClient, private router: Router, private jwtHelper: JwtHelperService) { }

  ngOnInit() {
    this.getProducts();    
    this.getPurchases();
  }

  ngAfterViewInit(): void {
    this.productData.sort = this.productSort;
    this.purchaseData.sort = this.purchaseSort;
  }

  public getProducts = () => {
    this.http.get("https://localhost:5001/product", {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }).subscribe(response => {
      this.productData.data = response as Product[];
    }, err => {
      console.log(err)
    });
  }

  public getPurchases = () => {
    this.http.get("https://localhost:5001/purchase", {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }).subscribe(response => {
      this.purchaseData.data = response as Purchase[];
    }, err => {
      console.log(err)
    });
  }

  isUserAuthenticated() {
    let token: string = localStorage.getItem("jwt");
    if (token && !this.jwtHelper.isTokenExpired(token)) {
      return true;
    }
    else {
      return false;
    }
  }

  public logOut = () => {
    localStorage.removeItem("jwt");
    this.router.navigate(["/login"]);
  }

  public purchaseProduct = (id: string) => {
    this.http.post("https://localhost:5001/purchase?productId=" + id, {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }).subscribe(response => {
      this.getPurchases();
    }, err => {
      console.log(err)
    });
  }

  public cancelPurchase = (id: string) => {
    this.http.delete("https://localhost:5001/purchase?purchaseId=" + id, {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }).subscribe(response => {
      this.getPurchases();
    }, err => {
      console.log(err)
    });
  }

}
