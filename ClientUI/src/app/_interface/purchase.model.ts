export interface Purchase {
    id: number,
    name: string,
    price: number,
    purchaseDate: Date,
    cancelled: boolean,
    cancelledDate: Date
}